<?php
return [
    'DEFAULT_CONTROLLER' => 'index',
    'DEFAULT_ACTION' => 'index',
    'DEFAULT_MODULE' => 'home',

    'CONTROLLER_TAG' => 'c',
    'ACTION_TAG'     => 'a',
    'MODULE_TAG'     => 'm'
];