<?php

namespace core\miraclePHP;
class Tool
{
    /**
     * 调试的方法
     * @param $data
     */
    public static function pre($data){
        echo '<pre style="
border: 1px solid grey;
background-color: lightgrey;
padding: 10px;
border-radius: 5px;
color: black;
font-size: 25px;
font-family: "微软雅黑", "宋体", Arial, Helvetica, sans-serif;

">';
        print_r($data);
        echo '</pre>';
    }


    /**
     * 获取数据的方法
     * @param $key
     * @param string $default
     * @param string $type
     * @return null|string
     */
    public static function  getData($key,$default='',$type=''){
        $data = '';
        if($type == ''){
            if($_SERVER['REQUEST_METHOD'] == 'POST')
                $data = isset($_POST[$key])? $_POST[$key] : $default;
            elseif($_SERVER['REQUEST_METHOD'] == 'GET')
                $data = isset($_GET[$key])? $_GET[$key] : $default;
        }elseif($type == 'post'){
            $data = isset($_POST[$key])? $_POST[$key] : $default;
        }elseif($type == 'get'){
            $data = isset($_GET[$key])? $_GET[$key] : $default;
        }else{
            return null;
        }

        $data = trim($data);//去掉空格
        $data = addslashes($data);//给特殊字符加上反斜杠(转义)
        $data = htmlspecialchars($data);//转义html代码,防xss
        return $data;
    }
}


