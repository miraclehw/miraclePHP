<?php
/**
 * Created by Teacher黄.
 * User: Administrator
 * Date: 2017/9/19
 * Time: 22:05
 * ===========
 * 用来加载配置文件
 * ===========
 */

namespace core\miraclePHP;


class Conf
{

    /**
     * 获取配置的方法
     * @param $name
     * @param $file
     * @return null
     */
    public static function get($name,$file){
        $path = ROOT.'/config/'.$file.'.php';
        if(file_exists($path)){
            $conf = include $path;
            if(array_key_exists($name,$conf))
                return $conf[$name];
            return null;
        }
        return null;
    }

}