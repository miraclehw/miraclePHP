<?php
/**
 * Created by Teacher黄.
 * User: Administrator
 * Date: 2017/9/19
 * Time: 15:37
 */

namespace core\miraclePHP;

class Route
{

    private $controller;//控制器
    private $action;//方法
    private $module;//模块

    public function __get($name)
    {
        if(isset($this->$name))
            return $this->$name;
        return null;
    }

    public function __construct()
    {
        $this->initController();
        $this->initAction();
    }

    private function initController(){
        //得到控制器
        $this->controller = Tool::getData(Conf::get('CONTROLLER_TAG','conf'),Conf::get('DEFAULT_CONTROLLER','conf'));

    }

    private function initAction(){
        //得到方法
        $this->action = Tool::getData(Conf::get('ACTION_TAG','conf'),Conf::get('DEFAULT_ACTION','conf'));
    }

}