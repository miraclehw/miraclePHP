<?php
/**
 * Created by Teacher黄.
 * User: Administrator
 * Date: 2017/9/19
 * Time: 17:30
 */

namespace core\miraclePHP;


use think\Exception;

trait View
{

    private $asign = [];
    public function assign($name,$value){
        $this->asign[$name] = $value;
    }

    public function display($file){
        $viewPath = APP.'views/'.$file.'.html';
        if(file_exists($viewPath)){
            extract($this->asign);
            include $viewPath;
        }else{
            throw new Exception('模板不存在!');
        }
    }

}