<?php
namespace core;
use core\miraclePHP\Route;
use core\miraclePHP\Tool;

class MiraclePHP
{


    /**
     * 自动加载类的方法
     * @param $className 类名 带命名空间
     * @throws \Exception
     */
    public static function load($className){
        # core\lib\A
        $className = str_replace('\\','/',$className);
        # core/lib/A.php
        $classPath = ROOT.'/'.$className.'.php';
        if(file_exists($classPath))
            include $classPath;
        else
            throw new \Exception('没有'.$classPath.'类文件');
    }


    public static function run(){
        # 自动加载类
        spl_autoload_register('\core\MiraclePHP::load');

        #  解析路由
        $route = new Route();

        # 示例化控制器
        $path = APP.'controller/'.ucfirst($route->controller).'Controller.php';
        if(file_exists($path)){
            include $path;
            $controller = '\app\controller\\'.ucfirst($route->controller).'Controller';
            $controller = new $controller();

            # 调用方法
            $action = $route->action;
            $controller->$action();
        }else
            throw new \Exception('控制器不存在!');
    }

}