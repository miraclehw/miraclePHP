<?php
/**
 * ===============================
 * 入口文件
 * 1.定义常量
 * 2.启动框架
 * ===============================
 */

define('ROOT',realpath(__DIR__.'/../'));//框架根目录
define('CORE',ROOT.'/core/');//核心目录
define('APP',ROOT.'/app/');//应用目录
include ROOT.'/vendor/autoload.php';
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();
include CORE.'/MiraclePHP.php';

\core\MiraclePHP::run();//启动框架